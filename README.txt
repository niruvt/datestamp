----------------------------------------------------------
Package:      datestamp
Version:      v0.1 (22 October, 2021)
Author:       निरंजन
Description:  Fixed date-stamps with LuaLaTeX
Repository:   https://gitlab.com/niruvt/datestamp
License:      GPLv3+, GFDLv1.3+
----------------------------------------------------------
