module     = "datestamp"
pkgversion = "0.1"
pkgdate    = os.date("%Y-%m-%d")

-- Tagging:
tagfiles = {"datestamp.dtx", "README.txt", "datestamp.ins"}
function update_tag(file, content, tagname, tagdate)
   if tagname == nil then
      tagname = pkgversion
      tagdate = pkgdate
   end
   if string.match(content,"Version:      v%d+%.%d+%w? %(%d+ %a+, %d+%)\n") then
      content = string.gsub(content,"Version:      v%d+%.%d+%w? %(%d+ %a+, %d+%)\n",
            "Version:      v" .. pkgversion .. " (" .. os.date("%d %B, %Y") .. ")\n")
   end
   if string.match(content,"\\def\\datestampversion{%d+%.%d+%w?}\n") then
      content = string.gsub(content,"\\def\\datestampversion{%d+%.%d+%w?}\n",
            "\\def\\datestampversion{" .. pkgversion .. "}\n")
   end
   if string.match(content,"\\def\\datestampdate{%d+/%d+/%d+}\n") then
      content = string.gsub(content,"\\def\\datestampdate{%d+/%d+/%d+}\n",
            "\\def\\datestampdate{" .. pkgdate .. "}\n")
   end
   if string.match(content,"LaTeX Package datestamp v%d+%.%d+%w?\n") then
      content = string.gsub(content,"LaTeX Package datestamp v%d+%.%d+%w?\n",
            "LaTeX Package datestamp v" .. pkgversion .. "\n")
   end
   return content
end

-- Checking:
checkengines = { "luatex" }
stdengine    = "luatex"
checkruns    = 3

-- Documentation:
typesetexe   = "lualatex"
typesetruns  = 2
typesetsuppfiles = { "gfdl-tex.tex", "datestamp-example.ds" }
typesetfiles = { "datestamp.dtx", "datestamp-example.tex" }
docfiles     = { "COPYING" }

-- CTAN upload
ctanreadme    = "README.txt"
uploadconfig  = {
   pkg         = module,
   author      = "निरंजन",
   email       = "hi.niranjan@pm.me",
   uploader    = "निरंजन",
   version     = pkgversion .. " " .. pkgdate,
   license     = "GPLv3+, GFDLv1.3+",
   summary     = "Fixed date-stamps with LuaLaTeX.",
   topic       = "date-time, luatex",
   ctanPath    = "/macros/luatex/latex/datestamp",
   repository  = "https://gitlab.com/niruvt/datestamp",
   bugtracker  = "https://gitlab.com/niruvt/datestamp/-/issues",
   update      = false,
   description = [[This package provides a LuaLaTeX-based method for typesetting fixed dates with the help of an auxiliary file. One can choose to write the custom dates manually in the auxiliary file and the results will be correctly displayed.]]
}
